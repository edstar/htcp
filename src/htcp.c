/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* mmap support */
#define _DEFAULT_SOURCE 1 /* MAP_POPULATE support */
#define _BSD_SOURCE 1 /* MAP_POPULATE for glibc < 2.19 */

#include "htcp.h"

#include <rsys/dynamic_array_double.h>
#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/mem_allocator.h>

#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

enum { X, Y, Z, TIME }; /* Helper constants */

struct htcp {
  int64_t pagesize;
  int8_t irregular_z;
  int32_t definition[4];
  double lower[3];
  double upper[3];
  double vxsz[2]; /* Size of the voxels in X and Y */
  struct darray_double vxsz_z; /* Size of the voxels along the Z dimension */
  struct darray_double coord_z; /* Lower coordinate of the voxel along Z */

  double* RCT; /* Mapped memory */
  double* RIT; /* Mapped memory */
  double* Reff_liq; /* Mapped memory */
  double* Reff_ice; /* Mapped memory */
  double* RVT; /* Mapped memory */
  double* PABST; /* Mapped memory */
  double* T; /* Mapped memory */
  size_t RCT_length; /* In bytes */
  size_t RIT_length; /* In bytes */
  size_t Reff_liq_length; /* In bytes */
  size_t Reff_ice_length; /* In bytes */
  size_t RVT_length; /* In bytes */
  size_t PABST_length; /* In bytes */
  size_t T_length; /* In bytes */

  size_t pagesize_os; /* Page size of the os */
  int verbose; /* Verbosity level */
  struct logger* logger;
  struct mem_allocator* allocator;
  ref_T ref;
};

/*******************************************************************************
 * Local functions
 ******************************************************************************/
static void
log_msg
  (const struct htcp* htcp,
   const enum log_type stream,
   const char* msg,
   va_list vargs)
{
  ASSERT(htcp && msg);
  if(htcp->verbose) {
    res_T res; (void)res;
    res = logger_vprint(htcp->logger, stream, msg, vargs);
    ASSERT(res == RES_OK);
  }
}

static void
log_err(const struct htcp* htcp, const char* msg, ...)
{
  va_list vargs_list;
  ASSERT(htcp && msg);
  va_start(vargs_list, msg);
  log_msg(htcp, LOG_ERROR, msg, vargs_list);
  va_end(vargs_list);
}

static void
reset_htcp(struct htcp* htcp)
{
  ASSERT(htcp);
  htcp->pagesize = 0;
  htcp->irregular_z = 0;
  htcp->definition[0] = 0;
  htcp->definition[1] = 0;
  htcp->definition[2] = 0;
  htcp->lower[0] = -1;
  htcp->lower[1] = -1;
  htcp->lower[2] = -1;
  htcp->vxsz[0] = -1;
  htcp->vxsz[1] = -1;
  darray_double_clear(&htcp->vxsz_z);
  darray_double_clear(&htcp->coord_z);
  if(htcp->RCT) munmap(htcp->RCT, htcp->RCT_length);
  if(htcp->RIT) munmap(htcp->RIT, htcp->RIT_length);
  if(htcp->Reff_liq) munmap(htcp->Reff_liq, htcp->Reff_liq_length);
  if(htcp->Reff_ice) munmap(htcp->Reff_ice, htcp->Reff_ice_length);
  if(htcp->RVT) munmap(htcp->RVT, htcp->RVT_length);
  if(htcp->PABST) munmap(htcp->PABST, htcp->PABST_length);
  if(htcp->T) munmap(htcp->T, htcp->T_length);
  htcp->RCT = NULL;
  htcp->RIT = NULL;
  htcp->Reff_liq = NULL;
  htcp->Reff_ice = NULL;
  htcp->RVT = NULL;
  htcp->RCT_length = 0;
  htcp->RIT_length = 0;
  htcp->Reff_liq_length = 0;
  htcp->Reff_ice_length = 0;
  htcp->RVT_length = 0;
}

static res_T
load_stream(struct htcp* htcp, FILE* stream, const char* stream_name)
{
  size_t nz = 0;
  size_t map_len = 0;
  size_t filesz;
  off_t offset = 0;
  res_T res = RES_OK;
  ASSERT(htcp && stream && stream_name);

  reset_htcp(htcp);

  #define READ(Var, N, Name) {                                                 \
    if(fread((Var), sizeof(*(Var)), (N), stream) != (N)) {                     \
      log_err(htcp, "%s: could not read the %s\n", stream_name, Name);         \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&htcp->pagesize, 1, "page size");
  if(!IS_POW2(htcp->pagesize)) {
    log_err(htcp, "%s: invalid page size `%li'. It must be a power of two.\n",
      stream_name, (long)htcp->pagesize);
    res = RES_BAD_ARG;
    goto error;
  }

  if(!IS_ALIGNED(htcp->pagesize, htcp->pagesize_os)) {
    log_err(htcp,
"%s: invalid page size `%li'. The page size attribute must be aligned on the "
"page size of the operating system , i.e. %lu.\n",
      stream_name, (long)htcp->pagesize,  (unsigned long)htcp->pagesize_os);
    res = RES_BAD_ARG;
    goto error;
  }

  READ(&htcp->irregular_z, 1, "'irregular Z' flag");
  READ(htcp->definition, 4, "spatial and time definitions");
  if(htcp->definition[0] <= 0
  || htcp->definition[1] <= 0
  || htcp->definition[2] <= 0
  || htcp->definition[3] <= 0) {
    log_err(htcp,
"%s: the spatial/time definition cannot be negative or null -- spatial "
"definition: %i %i %i; time definition: %i\n",
      stream_name, SPLIT3(htcp->definition), htcp->definition[TIME]);
    res = RES_BAD_ARG;
    goto error;
  }

  READ(htcp->lower, 3, "lower position");
  READ(htcp->vxsz, 2, "XY voxel size ");

  nz = htcp->irregular_z ? (size_t)htcp->definition[Z] : 1;
  res = darray_double_resize(&htcp->vxsz_z, nz);
  if(res != RES_OK) {
    log_err(htcp,
      "%s: could not allocate memory to store the size of the voxels in Z.\n",
      stream_name);
    goto error;
  }
  READ(darray_double_data_get(&htcp->vxsz_z), nz, "Z voxel size(s)");
  #undef READ

  htcp->upper[0] = htcp->lower[0] + htcp->vxsz[0] * htcp->definition[0];
  htcp->upper[1] = htcp->lower[1] + htcp->vxsz[1] * htcp->definition[1];
  if(!htcp->irregular_z) {
    htcp->upper[2] = htcp->lower[2]
      + darray_double_cdata_get(&htcp->vxsz_z)[0] * htcp->definition[2];
  } else {
    /* Compute the Z lower bound in Z of each Z slice */
    const double* size = NULL;
    double* coord = NULL;
    size_t i;
    res = darray_double_resize(&htcp->coord_z, nz);
    if(res != RES_OK) {
      log_err(htcp, "%s: could not allocate memory to store the lower "
        "coordinate of the voxels in Z.\n", FUNC_NAME);
      goto error;
    }
    size  = darray_double_cdata_get(&htcp->vxsz_z);
    coord = darray_double_data_get(&htcp->coord_z);
    FOR_EACH(i, 0, nz) coord[i] = i ? coord[i-1] + size[i-1] : htcp->lower[2];
    htcp->upper[2] = coord[nz-1] + size[nz-1];
  }

  map_len =
    (size_t)htcp->definition[X]
  * (size_t)htcp->definition[Y]
  * (size_t)htcp->definition[Z]
  * (size_t)htcp->definition[TIME]
  * sizeof(double);
  /* Align sizeof mapped data onto page size */
  map_len = ALIGN_SIZE(map_len, (size_t)htcp->pagesize);

  /* Ensure that offset is align on page size */
  offset = ALIGN_SIZE(ftell(stream), htcp->pagesize);

  fseek(stream, 0, SEEK_END);
  filesz = (size_t)ftell(stream);

  #define MMAP(Var) {                                                          \
    if((size_t)offset + map_len > filesz) {                                    \
      log_err(htcp, "%s: coult not load the "STR(Var)" data.\n", stream_name); \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
    htcp->Var = mmap(NULL, map_len, PROT_READ, MAP_PRIVATE|MAP_POPULATE,       \
      fileno(stream), offset);                                                 \
    if(htcp->Var == MAP_FAILED) {                                              \
      log_err(htcp, "%s: could not map the "STR(Var)" data -- %s.\n",          \
        stream_name, strerror(errno));                                         \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
    htcp->Var##_length = map_len;                                              \
  } (void)0
  MMAP(RVT); offset += (off_t)map_len;
  MMAP(RCT); offset += (off_t)map_len;
  MMAP(RIT); offset += (off_t)map_len;
  MMAP(Reff_liq); offset += (off_t)map_len;
  MMAP(Reff_ice); offset += (off_t)map_len;
  MMAP(PABST); offset += (off_t)map_len;
  MMAP(T); offset += (off_t)map_len;
  #undef MMAP

  CHK(fseek(stream, offset, SEEK_CUR) != -1);

exit:
  return res;
error:
  goto exit;
}

static void
release_htcp(ref_T* ref)
{
  struct htcp* htcp;
  ASSERT(ref);
  htcp = CONTAINER_OF(ref, struct htcp, ref);
  reset_htcp(htcp);
  darray_double_release(&htcp->vxsz_z);
  darray_double_release(&htcp->coord_z);
  MEM_RM(htcp->allocator, htcp);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
htcp_create
  (struct logger* in_logger,
   struct mem_allocator* mem_allocator,
   const int verbose,
   struct htcp** out_htcp)
{
  struct htcp* htcp = NULL;
  struct mem_allocator* allocator = NULL;
  struct logger* logger = NULL;
  res_T res = RES_OK;

  if(!out_htcp) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  logger = in_logger ? in_logger : LOGGER_DEFAULT;

  htcp = MEM_CALLOC(allocator, 1, sizeof(struct htcp));
  if(!htcp) {
    if(verbose) {
      logger_print(logger, LOG_ERROR,
        "%s: could not allocate the HTCP handler.\n", FUNC_NAME);
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&htcp->ref);
  htcp->allocator = allocator;
  htcp->logger = logger;
  htcp->verbose = verbose;
  htcp->pagesize_os = (size_t)sysconf(_SC_PAGESIZE);
  darray_double_init(htcp->allocator, &htcp->vxsz_z);
  darray_double_init(htcp->allocator, &htcp->coord_z);

exit:
  if(out_htcp) *out_htcp = htcp;
  return res;
error:
  if(htcp) {
    HTCP(ref_put(htcp));
    htcp = NULL;
  }
  goto exit;
}

res_T
htcp_ref_get(struct htcp* htcp)
{
  if(!htcp) return RES_BAD_ARG;
  ref_get(&htcp->ref);
  return RES_OK;
}

res_T
htcp_ref_put(struct htcp* htcp)
{
  if(!htcp) return RES_BAD_ARG;
  ref_put(&htcp->ref, release_htcp);
  return RES_OK;
}

res_T
htcp_load(struct htcp* htcp, const char* path)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!htcp || !path) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(htcp, "%s: error opening file `%s'.\n", FUNC_NAME, path);
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(htcp, file, path);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
htcp_load_stream(struct htcp* htcp, FILE* stream)
{
  if(!htcp || !stream) return RES_BAD_ARG;
  return load_stream(htcp, stream, "<stream>");
}

res_T
htcp_get_desc(const struct htcp* htcp, struct htcp_desc* desc)
{
  if(!htcp || !desc) return RES_BAD_ARG;
  if(!htcp->RVT) return RES_BAD_ARG;
  desc->pagesize = (size_t)htcp->pagesize;
  desc->irregular_z = htcp->irregular_z != 0;
  desc->spatial_definition[0] = (size_t)htcp->definition[X];
  desc->spatial_definition[1] = (size_t)htcp->definition[Y];
  desc->spatial_definition[2] = (size_t)htcp->definition[Z];
  desc->time_definition = (size_t)htcp->definition[TIME];
  desc->lower[0] = htcp->lower[0];
  desc->lower[1] = htcp->lower[1];
  desc->lower[2] = htcp->lower[2];
  desc->upper[0] = htcp->upper[0];
  desc->upper[1] = htcp->upper[1];
  desc->upper[2] = htcp->upper[2];
  desc->vxsz_x = htcp->vxsz[0];
  desc->vxsz_y = htcp->vxsz[1];
  desc->vxsz_z = darray_double_cdata_get(&htcp->vxsz_z);
  desc->coord_z = htcp->irregular_z ? darray_double_cdata_get(&htcp->coord_z) : NULL;
  desc->RCT = htcp->RCT;
  desc->RIT = htcp->RIT;
  desc->Reff_liq = htcp->Reff_liq;
  desc->Reff_ice = htcp->Reff_ice;
  desc->RVT = htcp->RVT;
  desc->PABST = htcp->PABST;
  desc->T = htcp->T;
  return RES_OK;
}

