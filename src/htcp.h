/* Copyright (C) 2018, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTCP_H
#define HTCP_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(HTCP_SHARED_BUILD) /* Build shared library */
  #define HTCP_API extern EXPORT_SYM
#elif defined(HTCP_STATIC) /* Use/build static library */
  #define HTCP_API extern LOCAL_SYM
#else /* Use shared library */
  #define HTCP_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the htcp function `Func'
 * returns an error. One should use this macro on htcp function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define HTCP(Func) ASSERT(htcp_ ## Func == RES_OK)
#else
  #define HTCP(Func) htcp_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

/* Forward declaration of opaque data types */
struct htcp;

struct htcp_desc {
  size_t pagesize; /* In bytes */
  int irregular_z;

  /* #coordinates along the X, Y and Z axis. X means for the West-Est axis, Y
   * is the South-North axis and finally Z is the altitude */
  size_t spatial_definition[3];
  size_t time_definition; /* Definition of the time */

  double lower[3]; /* Lower position of the grid in meters */
  double upper[3]; /* Upper position of the grid in meters */
  double vxsz_x; /* Voxel size in X in meters */
  double vxsz_y; /* Voxel size in Y in meters */
  const double* vxsz_z; /* Voxel size along Z in meters */
  const double* coord_z; /* Voxel lower bound along Z. NULL if !irregular_z */

  const double* RCT;
  const double* RIT;
  const double* Reff_liq;
  const double* Reff_ice;
  const double* RVT;
  const double* PABST; /* Pressure */
  const double* T; /* Temperature */
};
#define HTCP_DESC_NULL__ \
  {0,-1,{0,0,0},0,{-1,-1,-1},{0,0,0},-1,-1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}
static const struct htcp_desc HTCP_DESC_NULL = HTCP_DESC_NULL__;

BEGIN_DECLS

/*******************************************************************************
 * HTCP API
 ******************************************************************************/
HTCP_API res_T
htcp_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct htcp** htcp);

HTCP_API res_T
htcp_load
  (struct htcp* htcp,
   const char* path);

HTCP_API res_T
htcp_load_stream
  (struct htcp* htcp,
   FILE* stream);

HTCP_API res_T
htcp_ref_get
  (struct htcp* htcp);

HTCP_API res_T
htcp_ref_put
  (struct htcp* htcp);

HTCP_API res_T
htcp_get_desc
  (const struct htcp* htcp,
   struct htcp_desc* desc);

/* Internal function */
static FINLINE double
htcp_dblgrid4D_at__
  (const double* grid,
   const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t t)
{
  size_t row, slice, array;
  ASSERT(desc && grid);
  ASSERT(x < desc->spatial_definition[0]);
  ASSERT(y < desc->spatial_definition[1]);
  ASSERT(z < desc->spatial_definition[2]);
  ASSERT(t < desc->time_definition);
  row = desc->spatial_definition[0];
  slice = desc->spatial_definition[1] * row;
  array = desc->spatial_definition[2] * slice;
  return grid[t*array + z*slice + y*row + x];
}

static FINLINE double
htcp_desc_RCT_at
  (const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t t)
{
  return htcp_dblgrid4D_at__(desc->RCT, desc, x, y, z, t);
}

static FINLINE double
htcp_desc_RIT_at
  (const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t t)
{
  return htcp_dblgrid4D_at__(desc->RIT, desc, x, y, z, t);
}

static FINLINE double
htcp_desc_Reff_liq_at
  (const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t t)
{
  return htcp_dblgrid4D_at__(desc->Reff_liq, desc, x, y, z, t);
}

static FINLINE double
htcp_desc_Reff_ice_at
  (const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t t)
{
  return htcp_dblgrid4D_at__(desc->Reff_ice, desc, x, y, z, t);
}

static FINLINE double
htcp_desc_RVT_at
  (const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t t)
{
  return htcp_dblgrid4D_at__(desc->RVT, desc, x, y, z, t);
}

static FINLINE double
htcp_desc_PABST_at
  (const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t t)
{
  return htcp_dblgrid4D_at__(desc->PABST, desc, x, y, z, t);
}

static FINLINE double
htcp_desc_T_at
  (const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z, const size_t t)
{
  return htcp_dblgrid4D_at__(desc->T, desc, x, y, z, t);
}

static FINLINE void
htcp_desc_get_voxel_aabb
  (const struct htcp_desc* desc,
   const size_t x, const size_t y, const size_t z,
   double lower[3],
   double upper[3])
{
  ASSERT(desc && lower && upper);
  ASSERT(x < desc->spatial_definition[0]);
  ASSERT(y < desc->spatial_definition[1]);
  ASSERT(z < desc->spatial_definition[2]);
  lower[0] = (double)x * desc->vxsz_x;
  lower[1] = (double)y * desc->vxsz_y;
  upper[0] = lower[0] + desc->vxsz_x;
  upper[1] = lower[1] + desc->vxsz_y;
  if(!desc->irregular_z) {
    lower[2] = (double)z * desc->vxsz_z[0];
    upper[2] = lower[2] + desc->vxsz_z[0];
  } else {
    lower[2] = desc->coord_z[z];
    upper[2] = lower[2] + desc->vxsz_z[z];
  }
}

END_DECLS

#endif /* HTCP_H */
